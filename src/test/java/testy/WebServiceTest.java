package testy;

import static org.junit.Assert.*;

import java.util.Random;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.hibernate.Session;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import database.Kierunek;
import database.Komentarz;
import database.MySessionFactory;
import database.Praca;
import database.Promotor;
import database.Student;
import database.Wydzial;
import dyplomy.DiplomaInterface;

public class WebServiceTest {

	@Test
	public void getTopicPropositionsAndAddTopicPropositionTest() {
		Random r = new Random();
		int title = r.nextInt();
		int desc = r.nextInt();
		DiplomaInterface client = getService();
		Praca [] actualTopics = client.getTopicPropositions(new Long(1));
		int actualSize = actualTopics==null ? 0 : actualTopics.length;
		client.addTopicProposition(new Long(1), Integer.toString(title), new Long(1), Integer.toString(desc));
		actualTopics = client.getTopicPropositions(new Long(1));
		assertEquals(actualSize+1, actualTopics.length);
		assertEquals(Integer.toString(title), actualTopics[actualTopics.length-1].getTytul());
		assertEquals(Integer.toString(desc), actualTopics[actualTopics.length-1].getOpis());
	}
	
	@Test
	public void refuseTopicPropositionTest() {
		Random r = new Random();
		int title = r.nextInt();
		int desc = r.nextInt();
		DiplomaInterface client = getService();
		client.addTopicProposition(new Long(1), Integer.toString(title), new Long(1), Integer.toString(desc));
		Praca [] actualTopics = client.getTopicPropositions(new Long(1));
		Long id = actualTopics[actualTopics.length-1].getId();
		client.refuseTopicProposition(id, "Test refuse comment");
		actualTopics = client.getTopicsByPromoter(new Long(1));
		for(Praca p : actualTopics) {
			if(p.getId() != id) continue;
			assertEquals(11, p.getStatus());
		}
		
		Komentarz [] komentarze = client.getTopicComments(id);
		assertEquals("Test refuse comment", komentarze[komentarze.length-1].getTresc());
		assertEquals(id, komentarze[komentarze.length-1].getPraca().getId());
	}
	
	@Test
	public void acceptPropositionTest() {
		Random r = new Random();
		DiplomaInterface client = getService();
		Praca [] actualTopics = client.getTopicPropositions(new Long(1));
		Long id = actualTopics[actualTopics.length-1].getId();
		client.acceptTopicProposition(id);
		actualTopics = client.getTopicsByPromoter(new Long(1));
		for(Praca p : actualTopics) {
			if(p.getId() != id) continue;
			assertEquals(1, p.getStatus());
		}
	}
	
	@Test
	public void addPredefinedTopicTest() {
		Random r = new Random();
		int title = r.nextInt();
		int desc = r.nextInt();
		DiplomaInterface client = getService();
		Praca [] actualTopics = client.getAllTopics();
		int actualSize = actualTopics==null?0:actualTopics.length;
		client.addPredefinedTopic(new Long(1), Integer.toString(title), Integer.toString(desc));
		actualTopics = client.getAllTopics();
		assertEquals(actualSize+1, actualTopics.length);
		assertEquals(actualTopics[actualTopics.length-1].getTytul(), Integer.toString(title));
		assertEquals(actualTopics[actualTopics.length-1].getOpis(), Integer.toString(desc));
	}
	
	@Test
	public void sendPropositionForCorrection() {
		Random r = new Random();
		int title = r.nextInt();
		int desc = r.nextInt();
		DiplomaInterface client = getService();
		client.addTopicProposition(new Long(1), Integer.toString(title), new Long(1), Integer.toString(desc));
		Praca [] actualTopics = client.getTopicPropositions(new Long(1));
		Long id = actualTopics[actualTopics.length-1].getId();
		client.sendPropositionForCorrection(id, "Test correction comment");
		Komentarz [] komentarze = client.getTopicComments(id);
		assertEquals("Test correction comment", komentarze[komentarze.length-1].getTresc());
		assertEquals(id, komentarze[komentarze.length-1].getPraca().getId());
	}
	
	@Test
	public void cancelTopicPropositionTest() {
		Random r = new Random();
		int title = r.nextInt();
		int desc = r.nextInt();
		DiplomaInterface client = getService();
		client.addTopicProposition(new Long(1), Integer.toString(title), new Long(1), Integer.toString(desc));
		Praca [] actualTopics = client.getTopicPropositions(new Long(1));
		Long id = actualTopics[actualTopics.length-1].getId();
		int size = actualTopics.length;
		client.cancelTopicProposition(id);
		actualTopics = client.getTopicsByPromoter(new Long(1));
		for(Praca p : actualTopics) {
			if(p.getId() != id) continue;
			assertEquals(p.getStatus(), 12);
		}
	}
	
	@Test
	public void getTopicCommentsTest() {
		Random r = new Random();
		int title = r.nextInt();
		int desc = r.nextInt();
		DiplomaInterface client = getService();
		client.addTopicProposition(new Long(1), Integer.toString(title), new Long(1), Integer.toString(desc));
		Praca [] actualTopics = client.getTopicPropositions(new Long(1));
		Long id = actualTopics[actualTopics.length-1].getId();
		client.sendPropositionForCorrection(id, "Test comment");
		client.sendPropositionForCorrection(id, "Test comment 2");
		Komentarz [] komentarze = client.getTopicComments(id);
		assertEquals("Test comment 2", komentarze[komentarze.length-1].getTresc());
		assertEquals("Test comment", komentarze[0].getTresc());
	}
	
	@Test
	public void submitForPredefinedTopicTest() {
		Random r = new Random();
		int title = r.nextInt();
		int desc = r.nextInt();
		DiplomaInterface client = getService();
		client.addPredefinedTopic(new Long(1), Integer.toString(title), Integer.toString(desc));
		Praca [] topics = client.getFreeTopics();
		Praca p = topics[topics.length-1];
		Long id = p.getId();
		client.submitForPredefinedTopic(id, new Long(1));
		topics = client.getAllTopics();
		for(Praca pp : topics) {
			if(pp.getId() == id) {
				assertEquals(pp.getStudent().getId(), new Long(1));
			}
		}
	}
	
	@Test
	public void updateTopicPropositionTest() {
		Random r = new Random();
		int title = r.nextInt();
		int desc = r.nextInt();
		DiplomaInterface client = getService();
		client.addTopicProposition(new Long(1), Integer.toString(title), new Long(1), Integer.toString(desc));
		Praca [] topics = client.getTopicPropositions(new Long(1));
		Praca topic = topics[topics.length -1];
		client.updateTopicProposition(topic.getId(), "test", "test2");
		topics = client.getTopicPropositions(new Long(1));
		topic = topics[topics.length -1];
		assertEquals(topic.getTytul(), "test");
		assertEquals(topic.getOpis(), "test2");

	}
	
	private DiplomaInterface getService() {
		JaxWsProxyFactoryBean fctr = new JaxWsProxyFactoryBean();
		fctr.setServiceClass(DiplomaInterface.class);
		fctr.setAddress("http://localhost:8080/projekt-0.0.1-SNAPSHOT/dyplomy");
		return (DiplomaInterface) fctr.create();
	}
	
	@BeforeClass
	public static void beforeClass() {
		Session s = MySessionFactory.getSession();
		Wydzial w = new Wydzial();
		w.setNazwa("Mechaniczny");
		
		Kierunek k = new Kierunek();
		k.setNazwa("Informatyka");
		k.setWydzial(w);
		
		Student st = new Student();
		st.setImie("Jacek");
		st.setNazwisko("Jackowski");
		st.setRokStudiow(2);
		st.setStopienStudiow(1);
		st.setKierunek(k);
		
		Promotor p = new Promotor();
		p.setImie("Janusz");
		p.setNazwisko("Cebula");
		p.setTytul("Dr. Inz");
		
        s.beginTransaction();
	        s.save(w);
	        s.save(k);
	        s.save(st);
	        s.save(p);
        s.getTransaction().commit();
	}
	@AfterClass
    public static void doYourOneTimeTeardown() {
        Session s = MySessionFactory.getSession();
        s.beginTransaction();
        s.createQuery("delete from Komentarz").executeUpdate();
        s.createQuery("delete from Praca").executeUpdate();
        s.createQuery("delete from Student").executeUpdate();
        s.createQuery("delete from Promotor").executeUpdate();
        s.createQuery("delete from Kierunek").executeUpdate();
        s.createQuery("delete from Wydzial").executeUpdate();
        s.getTransaction().commit();
    }   
}
