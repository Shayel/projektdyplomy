package dyplomy;

import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;

import database.Komentarz;
import database.Praca;
import database.MySessionFactory;
import database.Promotor;
import database.Student;

public class DiplomaInterfaceImpl implements DiplomaInterface{
	@Override
	public Praca[] getAllTopics() {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		Query q = session.createQuery("FROM Praca");
		List<Praca> result = q.list();
		Praca [] retval = new Praca[result.size()];
		retval = result.toArray(retval);
		session.getTransaction().commit();
		return retval;	
	}

	@Override
	public Praca[] getFreeTopics() {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		Query q = session.createQuery("FROM Praca where student is null and status = 0");
		List<Praca> result = q.list();
		Praca [] retval = new Praca[result.size()];
		retval = result.toArray(retval);
		session.getTransaction().commit();
		return retval;	
	}

	@Override
	public Praca[] getTopicsByPromoter(Long promoterId) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		Promotor p = (Promotor) session.get(Promotor.class, promoterId);
		Query q = session.createQuery("FROM Praca where promotor = :val").setParameter("val",  p);
		List<Praca> result = q.list();
		Praca [] retval = new Praca[result.size()];
		retval = result.toArray(retval);
		session.getTransaction().commit();
		return retval;	
	}

	@Override
	public void addPredefinedTopic(Long promoterId, String title, String description) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		Praca p = new Praca();
		p.setOpis(description);
		p.setTytul(title);
		p.setStatus(0);
		p.setStudent(null);
		p.setPromotor((Promotor)session.get(Promotor.class,  promoterId));
		session.save(p);
		session.getTransaction().commit();
	}

	@Override
	public void addTopicProposition(Long promoterId, String title, Long studentId, String description) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		Praca p = new Praca();
		p.setOpis(description);
		p.setTytul(title);
		p.setStatus(10);
		p.setStudent((Student)session.get(Student.class, studentId));
		p.setPromotor((Promotor)session.get(Promotor.class,  promoterId));
		session.save(p);
		session.getTransaction().commit();
	}

	@Override
	public void acceptTopicProposition(Long topicId) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		session.createQuery("update Praca set status = 1 where id = :val").setParameter("val", topicId).executeUpdate();	
		session.getTransaction().commit();
	}

	@Override
	public void setTopicFinished(Long topicId) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		session.createQuery("update Praca set status = 2 where id = :val").setParameter("val", topicId).executeUpdate();	
		session.getTransaction().commit();	
	}

	@Override
	public Praca[] getTopicPropositions(Long promoterId) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		Promotor p = (Promotor) session.get(Promotor.class, promoterId);
		Query q = session.createQuery("FROM Praca where promotor = :val and status = 10").setParameter("val",  p);
		List<Praca> result = q.list();
		Praca [] retval = new Praca[result.size()];
		retval = result.toArray(retval);
		session.getTransaction().commit();
		return retval;	
	}

	@Override
	public void refuseTopicProposition(Long topicId, String comment) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		session.createQuery("update Praca set status = 11 where id = :val").setParameter("val", topicId).executeUpdate();
		Praca p = (Praca) session.get(Praca.class, topicId);
		Komentarz k = new Komentarz();
		k.setData(new Date());
		k.setNazwa("");
		k.setTresc(comment);
		k.setPraca(p);
		k.setPromotor(p.getPromotor());
		session.save(k);
		session.getTransaction().commit();	
		
	}

	@Override
	public void sendPropositionForCorrection(Long topicId, String comment) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		session.createQuery("update Praca set status = 10 where id = :val").setParameter("val", topicId).executeUpdate();
		Praca p = (Praca) session.get(Praca.class, topicId);
		Komentarz k = new Komentarz();
		k.setData(new Date());
		k.setNazwa("");
		k.setTresc(comment);
		k.setPraca(p);
		k.setPromotor(p.getPromotor());
		session.save(k);
		session.getTransaction().commit();			
	}

	@Override
	public void cancelTopicProposition(Long topicId) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		session.createQuery("update Praca set status = 12 where id = :val").setParameter("val", topicId).executeUpdate();	
		session.getTransaction().commit();	
	}

	@Override
	public Komentarz[] getTopicComments(Long topicId) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		Praca p = (Praca) session.get(Praca.class, topicId);
		Query q = session.createQuery("FROM Komentarz where praca = :val").setParameter("val", p);
		List<Praca> result = q.list();
		Komentarz [] retval = new Komentarz[result.size()];
		retval = result.toArray(retval);
		session.getTransaction().commit();
		return retval;	
	}

	@Override
	public void updateTopicProposition(Long topicId, String newName, String newDescription) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		session.createQuery("update Praca set tytul = :val1, opis = :val2 where id = :val3")
		.setParameter("val1", newName)
		.setParameter("val2", newDescription)
		.setParameter("val3", topicId).executeUpdate();
		session.getTransaction().commit();		
	}

	
	@Override
	public void submitForPredefinedTopic(Long topicId, Long studentId) {
		Session session = MySessionFactory.getSession();
		session.beginTransaction();
		Student st = (Student) session.get(Student.class, studentId);
		session.createQuery("update Praca set student = :val").setParameter("val", st).executeUpdate();
		session.getTransaction().commit();
	}	
}
