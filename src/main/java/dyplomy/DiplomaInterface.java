package dyplomy;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.transaction.Transactional;

import database.Komentarz;
import database.Praca;
@WebService(name="dyplomy")
public interface DiplomaInterface {
	//PROMOTOR//
	@WebMethod
	@Transactional
	void addPredefinedTopic(@WebParam(name="promoterId")Long promoterId,
				  @WebParam(name="title")String title,
				  @WebParam(name="description")String description);
	
	@WebMethod
	@Transactional
	void acceptTopicProposition(@WebParam(name="topicId")Long topicId);
		
	@WebMethod
	@Transactional
	void setTopicFinished(@WebParam(name="topicId")Long topicId);
	
	@WebMethod
	@Transactional
	Praca [] getTopicPropositions(@WebParam(name="promoterId")Long promoterId);
	
	@WebMethod
	@Transactional
	void refuseTopicProposition(@WebParam(name="topicId")Long topicId, @WebParam(name="comment")String comment);
	
	@WebMethod
	@Transactional
	void sendPropositionForCorrection(@WebParam(name="topicId")Long topicId, @WebParam(name="comment")String comment);
	
	//STUDENT//	
	@WebMethod
	@Transactional
	void addTopicProposition(@WebParam(name="promoterId")Long promoterId,
				  @WebParam(name="title")String title,
				  @WebParam(name="studentId")Long studentId,
				  @WebParam(name="description")String description);
	
	@WebMethod
	@Transactional
	void cancelTopicProposition(@WebParam(name="topicId")Long topicId);
	
	@WebMethod
	@Transactional
	void updateTopicProposition(@WebParam(name="topicId")Long topicId, 
								@WebParam(name="newName")String newName,
								@WebParam(name="newDescription")String newDescription);
	
	@WebMethod
	@Transactional
	void submitForPredefinedTopic(@WebParam(name="topicId")Long topicId,
								  @WebParam(name="studentId")Long studentId);
	@WebMethod
	@Transactional
	Komentarz[] getTopicComments(@WebParam(name="topicId")Long topicId);
		
	@WebMethod
	@Transactional
	Praca[] getAllTopics();
	
	@WebMethod
	@Transactional
	Praca[] getFreeTopics();
	
	@WebMethod
	@Transactional
	Praca[] getTopicsByPromoter(@WebParam(name="promoterId")Long promoterId);
		
}
