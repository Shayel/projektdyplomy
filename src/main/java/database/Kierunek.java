package database;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

@Table(name="Kierunki")
public class Kierunek {
	@Id
	@GeneratedValue(generator="increment")
	@Column(name="id_kierunku")
	private Long id;	
	@XmlTransient
	private Set studenci = new HashSet();
	private Wydzial wydzial;
	private String nazwa;
	
	public Kierunek(){}
	public Kierunek(String nazwa, Wydzial w) {
		this.nazwa = nazwa;
		this.wydzial = w;
		w.dodajKierunek(this);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Wydzial getWydzial() {
		return this.wydzial;
	}
	public void setWydzial(Wydzial wydzial) {
		this.wydzial=wydzial;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	@XmlTransient
	public Set getStudenci() {
		return studenci;
	}
	public void setStudenci(Set studenci) {
		this.studenci = studenci;
	}
	
	public void dodajStudenta(Student st) {
		st.setKierunek(this);
		studenci.add(st);
	}
	
	
}
