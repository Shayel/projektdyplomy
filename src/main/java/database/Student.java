/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlTransient;
/**
 *
 * @author Tomek
 */
public class Student implements Serializable{
	
	@Id
    @GeneratedValue(generator="increment")
	@Column(name="id_studenta")
    private Long id;
	private String imie;
	private String nazwisko;
	private Kierunek kierunek;
	
	public Student(String imie, String nazwisko, Kierunek k, int rok_s, int stopien_s) {
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.setKierunek(k);
		this.setRokStudiow(rok_s);
		this.setStopienStudiow(stopien_s);
		k.dodajStudenta(this);
	}
	
	public Student() {}
    	
	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}


	private int rok_studiow;
	private int stopien_studiow;
	@XmlTransient
	private Set prace = new HashSet();
	
    public Long getId() {
		return id;
	}
	@XmlTransient
	public Set getPrace() {
		return prace;
	}

	public void dodajPrace(Praca p) {
		p.setStudent(this);
		prace.add(p);
	}
	
	public void setPrace(Set prace) {
		this.prace = prace;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Kierunek getKierunek() {
		return kierunek;
	}

	public void setKierunek(Kierunek kierunek) {
		this.kierunek = kierunek;
	}

	public int getRokStudiow() {
		return rok_studiow;
	}

	public void setRokStudiow(int rok_studiow) {
		this.rok_studiow = rok_studiow;
	}

	public int getStopienStudiow() {
		return stopien_studiow;
	}

	public void setStopienStudiow(int stopien_studiow) {
		this.stopien_studiow = stopien_studiow;
	}

}
