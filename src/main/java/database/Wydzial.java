package database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;
@Table(name="Wydzialy")
public class Wydzial implements Serializable {
	@Id
	@Column(name="id_wydzialu")
	@GeneratedValue(generator="increment")
	private Long id;
	private String nazwa;
	@XmlTransient
	private Set kierunki = new HashSet();
	public Wydzial() {}
	
	public Wydzial(String nazwa) {
		this.nazwa = nazwa;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	@XmlTransient
	public Set getKierunki() {
		return kierunki;
	}
	public void setKierunki(Set kierunki) {
		this.kierunki = kierunki;
	}
	public void dodajKierunek(Kierunek ki) {
		ki.setWydzial(this);
		kierunki.add(ki);
	}
	

}
