package database;

import java.util.Date;



public class Komentarz {
	private long id;
	private Promotor promotor;
	private Praca praca;
	private String nazwa;
	private String tresc;
	private Date data;
	
	public Komentarz() {}
	
	public Komentarz(Praca p1,Promotor pr, String nazwa, String tresc, Date data) {
		this.praca = p1;
		this.promotor = pr;
		this.nazwa = nazwa;
		this.tresc = tresc;
		this.data = data;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public Promotor getPromotor() {
		return promotor;
	}

	public void setPromotor(Promotor promotor) {
		this.promotor = promotor;
	}

	public Praca getPraca() {
		return praca;
	}

	public void setPraca(Praca praca) {
		this.praca = praca;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getTresc() {
		return tresc;
	}

	public void setTresc(String tresc) {
		this.tresc = tresc;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	};
	
	
}
