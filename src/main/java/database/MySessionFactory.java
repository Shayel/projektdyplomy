package database;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class MySessionFactory {
	
	public static Session getSession() {
		Configuration configuration = new Configuration();
			    configuration.configure();
			    ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
			            configuration.getProperties()).build();
			    SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			    
			    Session session = sessionFactory.openSession();
			    return session;
		
	}
}
