package database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlTransient;

public class Praca implements Serializable {
	@Id
	@GeneratedValue(generator="increment")
	@Column(name="id_pracy")
	private Long id;
	private Student student;

	private Promotor promotor;
	private String tytul;
	private String opis;
	private int status;
	
	public Praca() {}
	public Praca(Student s, Promotor p, String tytul, String opis, int status) {
		this.student = s;
		this.promotor = p;
		this.tytul = tytul;
		this.opis = opis;
		this.status = status;
	}
	public int getStatus() {
		return status;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}

	public Promotor getPromotor() {
		return promotor;
	}
	public void setPromotor(Promotor promotor) {
		this.promotor = promotor;
	}
	public String getTytul() {
		return tytul;
	}
	public void setTytul(String tytul) {
		this.tytul = tytul;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}

	public boolean isZatwierdzona() {
		if(status == 1){
			return true;
		}
		else
		return false;
	}
	
	public boolean isOdrzucona() {
		if(status == 2){
			return true;
		}
		else
		return false;
	} 
	public void setStatus(int status) {
		this.status = status;
	}
}
