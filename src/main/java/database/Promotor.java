package database;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlTransient;

public class Promotor {
	@Id
	@GeneratedValue(generator="increment")
	@Column(name="id_promotora")
	private Long id;	
	private String tytul;	
	@XmlTransient
	private Set prace = new HashSet();
	private String imie;
	private String nazwisko;
	
	public Promotor(String imie, String nazwisko, String tytul) {
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.setTytul(tytul);
	}
	
	public Promotor() {};
	
	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	@XmlTransient
	public Set getPrace() {
		return prace;
	}

	public void setPrace(Set prace) {
		this.prace = prace;
	}
	
	public void dodajPrace(Praca p) {
		p.setPromotor(this);
		prace.add(p);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

}
